#!/usr/bin/env python3
"""
Copyright (C) 2018  Leonardo Malik

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
import sys
import subprocess
import re
import os
import logging
import shutil

DEPENDENCIES = ("hdparm", "tee")

VERSION = "2.3"
# Compatible - but more importantly fully tested with - these desktop environment versions
GNOME_SHELL_VERSION = "3.26"
KDE_PLASMA_VERSION = "5.12"
GNOME = "Gnome Shell"
KDE = "KDE Plasma"
# Setting: ("description", requires root?,
#           supported by which desktop environment? as a constant or a tuple of constants)
SUPPORTED_SETTINGS = {
    "Monitor standby": ("turn your monitor off after a specific time", False, (GNOME, KDE)),
    "HDDs standby": ("put your hard disks in standby mode after some time", True, (GNOME, KDE)),
    "pikaur AUR helper": ("switch yaourt for the better pikaur", True, (GNOME, KDE)),
    "Default sorting order": ("Set the default sorting order to type so that folders and files are grouped together "
                              "with their own kind", False, GNOME),
    "Papirus icon theme": ("Installs and activates the pretty Papirus icon theme", True, GNOME),
    "Arc Gnome theme": ("Installs and activates the pretty Arc Gnome theme", True, GNOME),
    "VM cleanup": ("Removes unneeded applications so that the VM size is as minimal as possible", True, (GNOME, KDE)),
}

SUPPORTED_ARGUMENTS = {
    "help": "shows this help information",
    "debug": "enable debugging in the log file",
    "root": "show a list of customizable settings that require root privileges",
    "noroot": "show a list of customizable settings that don't need root privileges",
    "force": "force the execution of the script even if the version of the desktop environment isn't compatible",
}

AS_ROOT = True

SUPPORTED_TIMEOUTS = {"5m": 60, "10m": 120, "15m": 180, "20m": 240, "21m": 252,
                      "30m": 241, "1u": 242, "1u30m": 243, "2u": 244, "2u30m": 245, "3u": 246}

BAD_AUR_HELPERS = ("yaourt", "pacaur")

logging.basicConfig(
    level=logging.WARNING,
    format="%(levelname)s (%(asctime)s): %(message)s",
    filename="AntergosConfigurator.log"
)

# This variable will be set to true if --force is passed
forced_run = False

""" ====== Functions ====== """


def check_arguments(args):
    """Parses the argument(s). Scripts will exit after --help, --root or --noroot"""
    if len(args) > 1:
        for a in args:
            logging.debug("Argument given: " + a)

            # Shows help menu
            if a[2:] == "help":
                show_help()
            # Shows list of settings with root requirement
            elif a[2:] == "root":
                show_root_settings()
            # Shows list of settings without root requirement
            elif a[2:] == "noroot":
                show_noroot_settings()
            # Enable debugging
            elif a[2:] == "debug":
                logging.getLogger("").setLevel(logging.DEBUG)
                logging.debug("Debugging enabled")
            # Force run the script if Gnome Shell version isn't compatible
            elif a[2:] == "force":
                global forced_run
                forced_run = True
            # Show settings for Gnome Shell
            elif a[2:] == "gnome":
                show_gnome_settings()
            # Show settings for KDE Plasma
            elif a[2:] == "kde":
                show_kde_settings()
    else:
        logging.debug("No arguments given")


def show_help():
    print(text_in_bold("Version: ") + VERSION + " - open source licensed under GPL v3\n")
    print_green("Arguments:\n")

    for arg, desc in SUPPORTED_ARGUMENTS.items():
        print("{}\t{}".format(text_in_bold(arg), desc))

    print()

    print("This configurator script is for Antergos and helps you set up the basic things of your OS. "
          "To make it function more nicely. Only GNOME Shell and KDE Plasma are supported.\n")
    print_green("Supported settings:\n")

    for setting, info in SUPPORTED_SETTINGS.items():
        which_de = info[2] if type(info[2]) is not tuple else ", ".join(info[2])
        print("- {}: {} ({})".format(text_in_bold(setting), info[0], text_in_bold(which_de)))

    print()
    sys.exit()


def show_root_settings():
    print("\nThese settings require root privileges:")

    for setting, info in SUPPORTED_SETTINGS.items():
        if info[1] is True:
            print("- {}: {}".format(text_in_bold(setting), info[0]))
    sys.exit()


def show_noroot_settings():
    print("\nThese settings do not require root privileges:")

    for setting, info in SUPPORTED_SETTINGS.items():
        if info[1] is False:
            print("- {}: {}".format(text_in_bold(setting), info[0]))
    sys.exit()


def show_gnome_settings():
    pass


def show_kde_settings():
    pass


def is_virtual_machine():
    """Is the script run in a virtual machine instead of a bare metal OS?"""
    try:
        result = execute_warn("grep -q ^flags.*\ hypervisor /proc/cpuinfo && echo True")
        return True if result == "True" else False
    except subprocess.CalledProcessError:
        return False


def check_for_root():
    if is_root():
        logging.debug("User started script as root, not allowed")
        print_red(("-" * 60) + "\nScript was excuted with root privileges.\n"
                               "Please run it as your own user\n" + ("-" * 60) + "\n")
        sys.exit(1)
    else:
        logging.debug("Root privilige not detected, continuing...")


def check_for_dependencies():
    error = False
    for c in DEPENDENCIES:
        if shutil.which(c) is None:
            print_red("Dependency not met: {} - please install it first!".format(c))
            logging.error("Dependency not met: {}".format(c))
            error = True

    if error:
        sys.exit(1)


def check_desktop_environment_version():
    """Checks if the version of the desktop environment is compatible with the one that is compatible with the script"""

    if is_gnome():
        current_version = get_gnome_version()
        de_version = GNOME_SHELL_VERSION
        de = GNOME
    elif is_kde():
        current_version = get_kde_version()
        de_version = KDE_PLASMA_VERSION
        de = KDE
    # Unsupported DE
    else:
        print_red("Sorry, your desktop environment is not supported. Cannot continue")
        logging.error("======> Desktop environment not compatible <======")
        sys.exit(1)

    compatible_version = re.match("(\d).(\d+)", de_version)

    # The major verions need to be the same and the minor needs to be >= compatible version
    if not forced_run:
        if current_version[1] != compatible_version[1] or \
                (current_version[1] == compatible_version[1] and
                 current_version[2] < compatible_version[2]):
            print_red("This script has only been tested with {} {}! "
                      "Use --force to run it anyway at your own risk".format(de, de_version))
            logging.error("======> Desktop environment version not compatible: {}", current_version[0])
            sys.exit(1)
    else:
        logging.debug("Desktop environment version is not compatible: %s %s - but execution was forced",
                      de, current_version[0])


def get_gnome_version():
    """Returns the Gnome Shell version as a MatchObject instance or False when not Gnome Shell"""
    try:
        gnome_version = execute_warn("gnome-shell --version")

        return re.match(r"GNOME Shell (\d).(\d+)(.(\d+))?", gnome_version, re.I)
    except subprocess.CalledProcessError:
        return False


def get_kde_version():
    """Returns the version of KDE Plasma as a MatchObject instance or False when it's not KDE Plasma"""
    try:
        kde_version = execute_warn("plasmashell --version")

        return re.match(r"plasmashell (\d).(\d+)(.(\d+))?", kde_version, re.I)
    except subprocess.CalledProcessError:
        return False


def is_gnome():
    """Only for detection so supress all output"""
    try:
        execute_warn("gnome-shell --version &> /dev/null")
        logging.debug("is_gnome(): We are in Gnome Shell")

        return True
    except subprocess.CalledProcessError:
        return False


def is_kde():
    """Only for detection so supress all output"""
    try:
        execute_warn("plasmashell --version &> /dev/null")
        logging.debug("is_kde(): We are in KDE Plasma")

        return True
    except subprocess.CalledProcessError:
        return False


def is_root():
    """Are we in root mode?"""
    return True if os.getegid() == 0 else False


def execute(command, as_root=False):
    """
    Used to run installation/removal and other commands where
    it's okay to exit the application if the command failed
    """
    try:
        command = ("sudo " if as_root else "") + command
        result = subprocess.check_output("{}".format(command), shell=True)
        logging.debug("execute():  command '{}', result: \n{}".format(command, result.decode().strip()))
    except subprocess.CalledProcessError as ex:
        logging.debug("execute(): Couldn't run command, message: {}".format(ex))
        print_red("Couldn't run command. Message: {}".format(ex))
        sys.exit(1)

    return result.decode().strip()


def execute_warn(command, as_root=False):
    """
    Used to run installation/removal and other commands where
    it's NOT okay to exit the application if the command failed. This is usually
    needed when we need to check the exception status
    """
    command = ("sudo " if as_root else "") + command
    result = subprocess.check_output("{}".format(command), shell=True)
    logging.debug("execute_warn(): command '{}', result: \n{}".format(command, result.decode().strip()))

    return result.decode().strip()


def app_exists(name):
    """Is this application installed? Not compatible with non-application packages"""
    try:
        # &> /dev/null suppresses the output message because we don't need that many details
        execute_warn("which {} &> /dev/null".format(name))
        logging.debug("app_exists(): {} is installed on this system".format(name))
        return True
    except subprocess.CalledProcessError:
        logging.debug("app_exists(): {} isn't installed on this system".format(name))
        return False


def package_exists(name):
    """
    This is different from app_exists() in the way that it can check for package that are not applications.
    However, it has trouble detecting applications that where not installed via pacman, like tee & chmod
    """
    try:
        # &> /dev/null suppresses the output message because we don't need that many details
        execute_warn("pacman -Qi {} &> /dev/null".format(name))
        logging.debug("package_exists(): {}  is installed on this system".format(name))
        return True
    except subprocess.CalledProcessError:
        logging.debug("package_exists(): {} isn't installed on this system".format(name))
        return False


def install_package(name):
    """Install a package with pacman, or if not available, try from AUR"""
    try:
        print("Installing {}...".format(name), end=" ", flush=True)
        execute_warn("pacman -S --noconfirm --quiet {} &> /dev/null".format(name), True)
        print_green("success".format(name))
        logging.debug("Installed {} package successfully".format(name))
        return True
    except subprocess.CalledProcessError:
        logging.warning("Installing of package {} failed".format(name))
        logging.debug("Checking AUR after installation failure")

        # Try AUR
        if app_exists("yaourt"):
            aur_helper = "yaourt"
            noedit_option = ""
        elif app_exists("pacaur"):
            aur_helper = "pacaur"
            noedit_option = "--silent --noedit"
        elif app_exists("pikaur"):
            aur_helper = "sudo pikaur"
            noedit_option = "--noedit"
        else:
            return False

        try:
            # Here we're exceptionally passing sudo manually, because pacaur doesn't work the usual way with sudo pacaur
            execute_warn("{} -S --noconfirm {} {}".format(aur_helper, noedit_option, name))
            print_green("success".format(name))
            logging.debug("Installed {} package from AUR successfully".format(name))
            return True
        except subprocess.CalledProcessError:
            print_red("failed")
            logging.warning("AUR also didn't contain package {}".format(name))
            return False


def remove_package(name, skip_dependencies=False):
    """Remove a package. AUR is not supported at this time"""
    try:
        print("Removing {}...".format(name), end=" ")
        # Remove package and exclusive dependencies if skip_dependencies is not set
        deps = "" if skip_dependencies else "s"
        execute_warn("pacman -R{} --noconfirm {} &> /dev/null".format(deps, name), True)
        print_green("success".format(name))
        logging.debug("Removed {} package successfully".format(name))
    except subprocess.CalledProcessError:
        print_red("failed")
        logging.warning("Removing of package {} failed - maybe not installed?".format(name))


def enable_service(service):
    try:
        logging.debug("enable_service(): enabling %s service", service)
        execute_warn("systemctl enable {}.service &> /dev/null".format(service), AS_ROOT)
        logging.debug("enable_service(): service %s enabled successfully", service)
        return True
    except subprocess.CalledProcessError:
        logging.debug("enable_service(): enabling %s service failed", service)
        return False


def is_service_enabled(service):
    try:
        execute_warn("systemctl is-enabled {}.service".format(service))
        logging.debug("is_service_enabled(): service %s is enabled", service)
        return True
    except subprocess.CalledProcessError:
        logging.debug("is_service_enabled(): service %s is not enabled", service)
        return False


def start_service(service):
    try:
        logging.debug("start_service(): starting %s service", service)
        execute_warn("systemctl start {}.service &> /dev/null".format(service), AS_ROOT)
        logging.debug("start_service(): service %s started successfully", service)
        return True
    except subprocess.CalledProcessError:
        logging.debug("start_service(): starting %s service failed", service)
        return False


def is_service_started(service):
    try:
        execute_warn("systemctl is-active --quiet {}.service".format(service))
        logging.debug("is_service_started(): service %s is running", service)
        return True
    except subprocess.CalledProcessError:
        logging.debug("is_service_started(): service %s is not running", service)
        return False


def text_to_green(string):
    return "\033[0;32m{}\033[0m".format(string)


def print_green(string):
    print(text_to_green(string))


def text_to_red(string):
    return "\033[0;31m{}\033[0m".format(string)


def print_red(string):
    print(text_to_red(string))


def text_in_bold(string):
    return "\033[1m{}\033[0m".format(string)


def print_title(string):
    if 'title_number' not in globals():
        global title_number
        title_number = 1

    """ Title for new each new setting """
    print("\n\033[0;34m{}. {}\033[0;0m\n".format(title_number, string))
    title_number += 1


def print_change_error(msg):
    logging.warning("Could not change setting - {}".format(msg))
    print_red("{}. Please contact your administrator lol :P. \n"
              "Sorry, but it really didn't work...".format(msg))


def print_change_success():
    logging.debug("Setting successfully changed")
    print_green("The setting was successfully applied.")


def ask_for_change():
    result = input("Would you like to change this setting (y/N)?")

    if result.lower() == "y":
        logging.debug("User wants to change this setting")
        return True
    else:
        logging.debug("User doesn't want to change this setting")
        return False


def get_gnome_setting(path, setting):
    """gsetting setting retrieval function. Return digits as ints, strings as string, ..."""
    if not is_gnome():
        return

    result = execute("gsettings get {} {}".format(path, setting))

    if re.match("uint32", result) or re.match("\d+", result):
        return int(re.search("^(uint32 )?(\d+)", result)[2])
    elif re.match("'[a-z-]+'", result, re.I):
        return str(re.search("'([a-z-]+)'", result, re.I)[1])
    elif result == "true":
        return True
    elif result == "false":
        return False
    else:
        return result


def set_gnome_setting(path, setting, value):
    """Set a Gnome setting and return true or false on success or failure, by checking with get_gnome_setting()"""
    if not is_gnome():
        return

    value = "true" if value is True else value
    value = "false" if value is False else value
    execute("gsettings set {} {} {}".format(path, setting, value))

    if get_gnome_setting(path, setting) == value:
        return True
    else:
        return False


def _gnome_get_monitor_timeout():
    """Get only the seconds from the timeout setting"""
    timeout = get_gnome_setting("org.gnome.desktop.session", "idle-delay")
    logging.debug("_gnome_get_monitor_timeout(): monitor timeout is %s", timeout)
    return timeout


def _kde_get_monitor_timeout():
    """
    With KDE, things are a bit different. It has a monitor timeout setting for when
    connected to AC or when working on battery. We're returning both

    :return: ac and battery timeouts as a tuple, None when not set
    """
    config_file = os.getenv("HOME") + "/.config/powermanagementprofilesrc"
    config_ac_idletime = False
    config_battery_idletime = False
    ac_timeout = 0
    battery_timeout = 0

    with open(config_file) as file:
        for line in file:
            # We skip the low battery config, because that's a different category
            if line.startswith("[AC][DPMSControl]"):
                config_ac_idletime = True
                logging.debug("_kde_get_monitor_timeout(): AC monitor timeout found in config")
                continue
            elif line.startswith("[Battery][DPMSControl]"):
                config_battery_idletime = True
                logging.debug("_kde_get_monitor_timeout(): Battery monitor timeout found in config")
                continue
            # It's a different setting, so reset the config_idletime var
            elif re.match("\[\w+\]", line):
                config_ac_idletime = False
                config_battery_idletime = False
                continue

            # We're in the AC monitor section
            if config_ac_idletime and line.startswith("idleTime"):
                ac_timeout = int(re.search("idleTime=(\d+)", line)[1])
                config_ac_idletime = False
                logging.debug("_kde_get_monitor_timeout(): AC monitor timeout is %s",
                              ac_timeout)
            # We're in the battery monitor section
            elif config_battery_idletime and line.startswith("idleTime"):
                battery_timeout = int(re.search("idleTime=(\d+)", line)[1])
                config_battery_idletime = False
                logging.debug("_kde_get_monitor_timeout(): Battery monitor timeout is %s",
                              battery_timeout)

    return ac_timeout, battery_timeout


def _gnome_set_monitor_timeout(minutes):
    """
    Set the monitor timeout setting to a new value in seconds. Ask for minutes and convert to seconds.

    :param minutes: an integer in minutes
    :type minutes: int
    """
    seconds = minutes * 60
    success = set_gnome_setting("org.gnome.desktop.session", "idle-delay", seconds)

    if success:
        print_change_success()
    else:
        print_change_error("Could not set monitor timeout for an unknown reason")


def _kde_set_monitor_timeout(minutes):
    """
    Set the monitor timeout setting to a new value in seconds for both the AC and battery mode.
    Ask for minutes and convert to seconds.

    :param minutes: an integer in minutes
    :type minutes: int
    """
    seconds = minutes * 60
    config_file = os.getenv("HOME") + "/.config/powermanagementprofilesrc"

    with open(config_file, "r") as file:
        contents = "".join(file.readlines())
        ac_idletime_config_found = True if "[AC][DPMSControl]" in contents else False
        battery_idletime_config_found = True if "[Battery][DPMSControl]" in contents else False

        if ac_idletime_config_found:
            contents = re.sub(r"\[AC\]\[DPMSControl\]\sidleTime=\d+\s+",
                              r"[AC][DPMSControl]\nidleTime={}\n\n".format(seconds),
                              contents,
                              0,
                              re.I)
            logging.debug("AC monitor setting found, changing to %d seconds", seconds)
        else:
            contents = "[AC][DPMSControl]\nidleTime={}\n\n".format(seconds) + contents
            logging.debug("AC monitor setting not found, "
                          "creating and setting to %d seconds", seconds)

        if battery_idletime_config_found:
            contents = re.sub(r"\[Battery\]\[DPMSControl\]\sidleTime=\d+\s+",
                              r"[Battery][DPMSControl]\nidleTime={}\n\n".format(seconds),
                              contents,
                              0,
                              re.I)
            logging.debug("Battery monitor setting found, changing to %d seconds", seconds)
        else:
            contents = "[Battery][DPMSControl]\nidleTime={}\n\n".format(seconds) + contents
            logging.debug("Battery monitor setting not found, "
                          "creating and setting to %d seconds", seconds)

    with open(config_file, "w") as file:
        file.write(contents)

    print_change_success()


"""
        print_change_success()
    except:
        print_change_error("Could not set monitor timeout for an unknown reason")
"""


def check_monitor_timeout():
    """This function will handle the monitor timeout setting."""
    print_title("First up is the monitor powering off setting. This setting is interesting to reduce "
                "the power usage of your monitor(s) and increase the lifespan of it.")

    # In KDE we need to add the warning that one setting will be applied for both AC and battery modes
    if is_kde():
        print(text_in_bold("- Please note that a custom value will change "
                           "the monitor timeout for both AC and battery mode -\n"))

    if is_gnome():
        monitor_timeout = _gnome_get_monitor_timeout() / 60
        logging.debug("check_monitor_timeout(): Monitor timeout setting: {} minutes".
                      format(monitor_timeout))

        if monitor_timeout == 0:
            print_red("The monitor timeout setting is disabled! You should change it")
        elif monitor_timeout <= 20:
            print_green("This setting is currently set to {} minutes, which is already good".
                        format(monitor_timeout))
        else:
            print_red("The monitor timeout is set too high! I recommend a value between 1-20")

        if ask_for_change():
            value = int(input("Enter a new value in minutes (1-20): "))
            logging.debug("check_monitor_timeout(): User wants to change monitor timeout to {} minutes".format(value))

            while value > 20:
                logging.warning("check_monitor_timeout(): Too many minutes chosen: {}".format(value))
                value = int(input("Unrecommended timeout chosen. Please enter a value between 1 and 20 minutes: "))

            _gnome_set_monitor_timeout(value)
    elif is_kde():
        monitor_timeout_ac = _kde_get_monitor_timeout()[0] / 60
        monitor_timeout_battery = _kde_get_monitor_timeout()[1] / 60
        logging.debug("check_monitor_timeout(): Monitor timeout setting on AC mode: {} minutes - "
                      "in battery mode: {}".format(monitor_timeout_ac, monitor_timeout_battery))

        if monitor_timeout_ac == 0:
            print_red("The monitor timeout setting is disabled! You should change it")
        elif monitor_timeout_ac <= 20:
            print_green("This setting is currently set to {} minutes, which is already good".
                        format(monitor_timeout_ac))
        else:
            print_red("The monitor timeout is set too high! I recommend a value between 1-20")

        if ask_for_change():
            value = int(input("Enter a new value in minutes (1-20): "))
            logging.debug("check_monitor_timeout(): User wants to change monitor timeout to {} minutes".format(value))

            while value > 20:
                logging.warning("check_monitor_timeout(): Too many minutes chosen: {}".format(value))
                value = int(input("Unrecommended timeout chosen. Please enter a value between 1 and 20 minutes: "))

            _kde_set_monitor_timeout(value)


def which_drives_are_hdds():
    """
    Look for hard disk drives and create a dict of them with their serial number as value.
    Use the serial number and model to find the config file in /etc/udisks2 if it exists and add it to the dict

    Has only been tested with SATA drives!
    """

    result = execute("lsblk -dno name,rota,serial")
    result = result.split("\n")
    hddlist = {}

    for line in result:
        if re.match("sd[a-z]\s+1", line):
            # Remember device and serial and build the location of the file for later
            device = "/dev/" + line[0:3]
            serial = re.search("(\w+)$", line)[1]
            filename = "/etc/udisks2/" + _convert_to_filename(find_drive_model(device), serial)
            hddlist[device] = [serial, filename]
            logging.debug("which_drives_are_hdds(): New SATA HDD found: {} with serial number is '{}'".format(device,
                                                                                                              serial))
            logging.debug("which_drives_are_hdds(): Its file location should be '{}'".format(filename))

    return hddlist


def _convert_to_filename(model, serial):
    result = model.replace(" ", "-") + "-" + serial + ".conf"
    logging.debug("convert_to_filename(): {} and {} changed to {}".format(model, serial, result))
    return result


def find_drive_model(drive):
    if not re.match("^/dev/s|hd[a-z]$", drive):
        raise Exception("find_drive_model(): arg1 didn't get a proper device: {}".format(drive))

    model = execute("hdparm -I {} | grep \"Model Number\"".format(drive), AS_ROOT)
    model = re.search("\s+(\w+\s?\w*)$", model)
    logging.debug("find_drive_model(): drive model found: {}".format(model[1]))
    return model[1]


def read_file_as_root(file):
    result = execute("cat {}".format(file), AS_ROOT)
    logging.debug("read_file_as_root(): Configuration file reading done")
    return result


def write_file_as_root(file, contents):
    result = execute('echo "{}" | sudo tee {}'.format(contents, file), AS_ROOT)
    logging.debug("write_file_as_root(): Configuration file written")
    return result


def get_hdd_timeout(configfile):
    """
    Gets the standy timeout value from the config file and converts the hdparm value to time in seconds,
    see man hdparm for more info at the -S section
    """

    if not os.path.exists(configfile):
        logging.debug("get_hdd_timeout(): config file doesn't exists, exiting function")
        return 0

    result = read_file_as_root(configfile).split("\n")

    for line in result:
        # We only need the StandbyTimeout setting value, then convert the hdparm value to time.
        if re.match("StandbyTimeout=\d+", line):
            result = 0
            timeout = int(re.search("^StandbyTimeout=(\d+)$", line)[1])
            logging.debug("get_hdd_timeout(): timeout {} found in config file".format(timeout))

            # x * 5 sec,
            # Ex: 5 * 5 = 25 seconds
            if 1 <= timeout <= 240:
                result = timeout * 5
            # 241 is 1 unit - 251 is 11 units of 30,
            # so x unit(s) * 30 mins = time
            elif 241 <= timeout <= 251:
                result = (timeout - 240) * 30 * 60
            # 21 min
            elif timeout == 252:
                result = 1260
            # This one is vendor specific and we can't calculate the real timeout value,
            # so we pretend it's disabled
            elif timeout == 253:
                pass
            # 21 min 15 sec
            elif timeout == 255:
                result = 1260 + 15

    logging.debug("get_hdd_timeout(): timeout converted to {} seconds".format(result))
    return result


def set_hdd_timeout(configfile, timeout):
    """
    Stores the standy timeout value in the config file of the drive after it converts the time to an hdparm value,
    see man hdparm for more info at the -S section
    """

    # Convert to hdparm timeout
    timeout = SUPPORTED_TIMEOUTS[timeout]

    # Config file doesn't exist yet, create it
    if not os.path.exists(configfile):
        logging.debug("set_hdd_timeout(): config file doesn't exists, creating it")
        file_contents = "# See udisks(8) for the format of this file.\n\n[ATA]\nStandbyTimeout={}\n"
        result = write_file_as_root(configfile, file_contents.format(timeout))

        if result != "":
            print_change_success()
        else:
            print_change_error("Couldn't save the configuration file for an unknown reason")
    # Config file exists, so just alter it
    else:
        file_contents = read_file_as_root(configfile)

        # Replace setting
        if re.search("StandbyTimeout=\d+", file_contents):
            file_contents = re.sub("StandbyTimeout=\d+", "StandbyTimeout={}".format(timeout), file_contents)
        # Add setting after [ATA]
        else:
            file_contents = re.sub("\[ATA\]\n", "[ATA]\nStandbyTimeout={}\n".format(timeout), file_contents)

        result = write_file_as_root(configfile, file_contents)

        if result != "":
            print_change_success()
        else:
            print_change_error("Couldn't save the modified configuration file for an unknown reason")


def check_hdd_timeouts():
    """ This function will handle the timeout settings of HDDs. Needs root privs """
    print_title("Next up is the HDD standby setting. This is interesting to reduce "
                "the power usage of your hard disk drives and increase the lifespan.")

    # Pre-check
    if is_virtual_machine():
        print_red("This script is run in a virtual machine, skipping this part...")
        logging.debug("check_hdd_timeouts() is skipped because vm")
        return

    print(">>> I recommended a value between 15 minutes and 1 hour <<<")

    hdds = which_drives_are_hdds()

    # Another pre-check
    if len(hdds.items()) == 0:
        print_green("No hard disk drives found in this machine, skipping this part...")
        logging.debug("check_hdd_timeouts() is skipped because no rotational drives were found")
        return

    for disk, disk_value in hdds.items():
        timeout_in_seconds = get_hdd_timeout(disk_value[1])

        print("\nCurrently checking the setting for drive {}:".format(disk))

        # It's 0 or the reserved 254
        if timeout_in_seconds == 0:
            print_red("It has no standby option activated! You should change it")
        # This is a long timeout! Show hours too
        elif timeout_in_seconds >= 3600:
            print_green("It will go standby after {} hours and "
                        "{} minutes of no activity".format(*divmod(int(timeout_in_seconds / 60), 60)))
        # It's only seconds
        elif timeout_in_seconds < 60:
            print_red("It will go standby after {} seconds of no activity. That's very low and should be changed".
                      format(timeout_in_seconds))
        # It's in minutes (and seconds)
        else:
            # Below 5 minutes is not recommended so display it in red
            more_than_five = timeout_in_seconds / 60 >= 5
            is_one = timeout_in_seconds / 60 == 1
            print_it = print_green if more_than_five else print_red
            # A little more beauty to it
            mins = "minute" if is_one else "minutes"
            # And the recommendation!
            change_it = "" if more_than_five else ", you should increase it!"

            print_it("It will go standby after {} {min} and "
                     "{} seconds of no activity{change}".format(*divmod(timeout_in_seconds, 60),
                                                                min=mins,
                                                                change=change_it))

        if ask_for_change():
            print(text_in_bold("The supported values are (m stands for minutes, u for hours):"))
            print(", ".join(SUPPORTED_TIMEOUTS.keys()))

            timeout_value = input("Please make a choice: ")
            logging.debug("check_hdd_timeouts(): User wants to change HDD {} timeout to {}".format(disk, timeout_value))

            # We need to be sure the value is supported by hdparm
            while True:
                if timeout_value in SUPPORTED_TIMEOUTS.keys():
                    print("Good value: " + timeout_value)
                    set_hdd_timeout(disk_value[1], timeout_value)
                    break
                else:
                    logging.warning("check_hdd_timeouts(): Unsupported value")
                    timeout_value = input("That's an unsupported setting. Please enter a supported value: ")
                    logging.debug("check_hdd_timeouts(): New value entered: {}".format(timeout_value))


def check_replace_yaourt():
    """ This function will replace yaourt with pikaur """
    print_title("Replace the built-in yaourt application with the more full-featured pikaur. "
                "Since pacaur is no longer maintained, it has been removed. Also, pikaur clearly "
                "shows the version differences between updates. "
                "Check https://wiki.archlinux.org/index.php/AUR_helpers for more info.")

    # Pre-check
    if app_exists("pikaur"):
        print_green("pikaur is already installed on the system, skipping this part...")
        logging.debug("check_replace_yaourt() is skipped because pikaur is installed")
        return

    safe_to_remove = False
    logging.debug("pikaur isn't installed on the system")

    print("Would you like to install pikaur?")
    logging.debug("pikaur isn't installed on the system, asking to install")

    if ask_for_change():
        if install_package("pikaur-git"):
            safe_to_remove = True

    # Remove all "bad" AUR helpers
    if safe_to_remove:
        print("\npikaur was successfully installed, so now we can remove other AUR helpers")
        for aur_helper in BAD_AUR_HELPERS:
            if app_exists(aur_helper):
                print("AUR helper {} was found, would you like to remove it?".format(aur_helper))
                logging.debug("AUR helper {} was found".format(aur_helper))

                if ask_for_change():
                    remove_package(aur_helper)
    else:
        logging.debug("pikaur was not installed, so no detection for other AUR helpers was performed")


def check_default_sort_order():
    if not is_gnome():
        return

    print_title("Set the default sorting order in Nautilus from 'name' to 'type' so that folders "
                "are grouped together and the same for files. They are still sorted chronologically.")

    sort_setting = get_gnome_setting("org.gnome.nautilus.preferences", "default-sort-order")

    # Pre-check
    if sort_setting == "type":
        print_green("The default sorting order is already set to type, skipping this part...")
        logging.debug("check_default_sort_order() is skipped because already set to type")
        return

    print("Your current default sorting order is set to '{}'".format(sort_setting))

    if ask_for_change():
        if set_gnome_setting("org.gnome.nautilus.preferences", "default-sort-order", "type"):
            print_green("Default sorting order was successfully changed.")
        else:
            print_red("Changing the default sorting order for some reason failed")


def check_papirus_icon_theme():
    """Beautiful icons for Gnome"""
    if not is_gnome():
        return

    print_title("Install the beautiful papirus icon theme. Worth a try!")

    if not package_exists("papirus-icon-theme"):
        print("The Papirus icon theme was not found on your computer")

        if ask_for_change():
            install_package("papirus-icon-theme")

            print("The Papirus icon theme is not activated yet")

            if ask_for_change():
                if set_gnome_setting("org.gnome.desktop.interface", "icon-theme", "Papirus"):
                    print_green("The Papirus icon theme is now part of your desktop theme!")
    else:
        if get_gnome_setting("org.gnome.desktop.interface", "icon-theme") == "Papirus":
            print_green("The Papirus icon theme is already installed and activated, skipping this part...")
            logging.debug("check_papirus_icon_theme() is skipped because already installed and activated")
        else:
            print("You already have the Papirus icon theme installed, but it's not activated")

            if ask_for_change():
                if set_gnome_setting("org.gnome.desktop.interface", "icon-theme", "Papirus"):
                    print_green("The Papirus icon theme is now part of your desktop theme!")


def check_arc_theme():
    if not is_gnome():
        return

    print_title("The Arc Gnome theme is also very much worth a try! It's described as a flat theme "
                "with transparent elements. Check https://github.com/horst3180/arc-theme")

    if not package_exists("arc-gtk-theme"):
        print("Arc theme was not found on your computer")

        if ask_for_change():
            install_package("arc-gtk-theme")

            print("Arc theme is not activated yet")

            if ask_for_change():
                if set_gnome_setting("org.gnome.desktop.interface", "gtk-theme", "Arc-Darker"):
                    print_green("Arc theme is now your main theme!")
    elif get_gnome_setting("org.gnome.desktop.interface", "gtk-theme") != "Arc-Darker":
        print("You already have the Arc theme installed, but it's not activated")

        if ask_for_change():
            if set_gnome_setting("org.gnome.desktop.interface", "gtk-theme", "Arc-Darker"):
                print_green("The Arc theme is now your GTK theme!")
    else:
        print_green("Arc GTK theme is already installed and activated, skipping this part...")
        logging.debug("check_arc_theme() is skipped Arc theme already installed and activated")

    # We can't set the Gnome Shell theme via gsettings because it's an extension. We can at least
    # check if the user-theme extension is installed
    print("Checking if you have the user-theme extension installed... ", end="")

    if "user-theme@gnome-shell-extensions.gcampax.github.com" in \
            get_gnome_setting("org.gnome.shell", "enabled-extensions"):
        print_green(text_in_bold("Yes"))
        print(text_in_bold("\nThis script can't change the theme for Gnome Shell because it's done through "
                           "the user-themes extension. The good news is that you already have it installed. "
                           "Please set it manually in gnome-tweak-tool > Appearance"))
        logging.debug("user-theme@gnome-shell-extensions.gcampax.github.com extension is installed")
    else:
        print_red(text_in_bold("No"))
        print(text_in_bold("\nThis script can't change the theme for Gnome Shell because it's done through "
                           "the user-themes extension. You also don't have it installed, so please do that first, "
                           "then: please set it manually in gnome-tweak-tool > Appearance"))
        logging.debug("user-theme@gnome-shell-extensions.gcampax.github.com extension is not installed")

    """
    This doesn't work:
    
    if get_gnome_setting("org.gnome.shell.extensions.user-theme", "name") == "Arc-Dark":
        logging.debug("Arc Gnome Shell theme already activated on the machine")
        print_green("Arc Gnome Shell theme is already activated, skipping this part...")
    else:
        print("You already have the Arc theme installed, but it's not activated for Gnome Shell")

        if ask_for_change():
            if set_gnome_setting("org.gnome.shell.extensions.user-theme", "name", "Arc-Dark"):
                print_green("The Arc theme is now your Gnome Shell theme!")
    """


def check_virtual_machine_apps():
    """
    Checks if we're in a virtual machine and prompts to clean it up. This is useful after a new installation and the
    user wants a minimal (GUI) OS. Provides the option to quit the configuration script afterwards
    """
    print_title("If this is a virtual machine, we can clean up applications that you'll maybe not need, so that "
                "your VM is as minimal as possible")

    apps_to_clean = ("transmission-gtk", "transmission-qt", "gnome-maps", "gnome-calendar", "gnome-contacts",
                     "gnome-disk-utility", "gnome-music", "gnome-sound-recorder", "gnome-weather", "clementine",
                     "polari", "kopete", "brasero", "totem", "gnome-documents", "evolution", "vlc", "k3b")

    if is_virtual_machine():
        logging.debug("check_virtual_machine_apps(): Houston, we're in a VM")

        print("- Applications to remove -\n")
        installed_apps = []

        for a in apps_to_clean:
            if package_exists(a):
                print("{} is {}".format(a, text_in_bold("installed")))
                installed_apps.append(a)
            else:
                print("{} is not installed".format(a))

        print("\nIf installed the meta package antergos-kde-meta will also be uninstalled\n")

        if len(installed_apps) != 0 and ask_for_change():
            # Remove the meta package first
            if package_exists("antergos-kde-meta"):
                remove_package("antergos-kde-meta", True)

            for a in installed_apps:
                remove_package(a)

            print_green("\nFinished removing the applications.\n")
            logging.debug("Finished removing the applications.")
        else:
            print_green("\nThere are none of these applications installed, skipping this part...\n")
            logging.debug("There are none of these applications installed, skipping this part...")

        wants_to_quit = input("Would you like to quit the script already (y/N)? ")

        if wants_to_quit.lower() == "y":
            sys.exit()
    else:
        logging.debug("check_virtual_machine_apps(): This is not a VM")
        print_green("This is not a virtual machine, skipping this part...")


""" ====== Start execution ====== """


if __name__ == '__main__':
    try:
        if len(sys.argv) > 1:
            check_arguments(sys.argv)

        logging.debug("\n\n\n!!!!!!!!!! START SCRIPT EXECUTION !!!!!!!!!!")

        check_for_root()
        check_for_dependencies()
        check_desktop_environment_version()
        print(text_in_bold("We will now go over all the settings to check if they are set optimally"))

        check_virtual_machine_apps()
        check_monitor_timeout()
        check_hdd_timeouts()
        check_replace_yaourt()
        check_default_sort_order()
        check_papirus_icon_theme()
        check_arc_theme()

        # For readability in the log file
        logging.debug("!!!!!!!!!! STOP SCRIPT EXECUTION !!!!!!!!!!\n\n\n")
        # And a clean exit in the terminal
        print()
    except KeyboardInterrupt:
        logging.debug("Script aborted manually")
    except Exception as e:
        print_red("Exception occured: {}".format(e))
        print_red("Please run the file again with the --debug argument and "
                  "file a bug with AntergosConfigurator.log attached at "
                  "https://gitlab.com/isaakm/AntergosConfigurator")
        logging.error("Exception occured: {}".format(e))
