This configurator script is for Antergos and helps you set up the basic recommended things of your OS. Note that
these recommendations are my personal opinions. Whether you find them useful yourself matters up to you. In every case
you'll have the option to skip that setting

Currently compatible with Gnome Shell >= 3.26 and KDE Plasma >= 5.12

### Current features
- Gnome: installation of the Arc theme
- Gnome: installation of the beautiful papirus icons
- Gnome: set default sorting order to "type"
- All: Cleanup VM installations: remove as many unneeded applications to minimize the VM size
- All: set the standby timeout for your monitor to save energy and increase the lifetime of your pixelz
- All: set the standby timeout for your hard drives individually to save energy and increase lifetime
- All: install the pikaur AUR helper and remove the "bad" ones

### Usage

```
Download the AntergosConfigurator.py file to your computer
chmod +x it to make it executable
Run with ./AntergosConfigurator.py
```

### Debugging

```
Run with ./AntergosConfigurator.py --debug
A log file named AntergosConfigurator.log will be created in the same directory.
If you post a bug, please attach this file
```